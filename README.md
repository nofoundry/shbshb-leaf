# Shbshb Pixel Leaf

![Image](specimen.jpg)

Created in solidarity with the plane tree occupation in Karlsruhe starting on August 17th, 2023. Please check out the links below.

On the base font: Shbshb Pixel is a slanted serif pixel typeface, based on a very low grid resolution of the CMU Concrete Italic. It was designed during spring 2020 lockdown for Radio Shbshb, a collaborative radio project from Karlsruhe.

Have fun with it!

## About the author

Timothée Charon is a student at HfG Karlsruhe and is part of the „NoFoundry“, which publishes typefaces from students, as well as Radio Shbshb.

## Links

[Timothée Charon — Instagram](https://www.instagram.com/timstathee/)

[Karlsruher Platanen Bleiben — Instagram](https://www.karlsruher-platanen-bleiben.de/)

[Karlsruher Platanen Bleiben — Website](https://www.instagram.com/karlsruher_platanen_bleiben/)

[Karlsruher Platanen Bleiben — Telegram channel](https://t.me/s/karlsruher_platanen_bleiben)

## License

This Font Software is licensed under the SIL Open Font License, Version 1.1.
This license is enclosed in this repository, and is also available with a FAQ at
http://scripts.sil.org/OFL. 
For any questions, feel free to contact [Timothée ](tncharon@hfg-karlsruhe.de).

## Repository Layout

This font repository structure is inspired by [Unified Font Repository v0.3](https://github.com/unified-font-repository/Unified-Font-Repository).

## Publishing

This font is made available through the [NoFoundry](http://nofoundry.xyz/).
